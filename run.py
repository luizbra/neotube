#!/usr/bin/env python3

from neotube import create_app

if __name__ == "__main__":
    create_app().run(debug=True)
