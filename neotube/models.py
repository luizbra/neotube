from datetime import datetime

from flask_login import UserMixin

from neotube import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    videos = db.relationship("Video", backref="user", lazy=True)
    playlists = db.relationship("Playlist", backref="user", lazy=True)

    def __repr__(self):
        return f"User('{self.id}', '{self.username}')"

vid_pl_association = db.Table("vid_pl_association", db.Column("video_id", db.Integer, db.ForeignKey("video.id")), db.Column("playlist_id", db.Integer, db.ForeignKey("playlist.id")))

class Video(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    title = db.Column(db.String(80), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    description = db.Column(db.Text)
    thumbnail = db.Column(db.String(20), nullable=False, default="default.jpeg")
    video = db.Column(db.String(20), nullable=False)

    def __repr__(self):
        return f"Video('{self.id}', '{self.title}')"

class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    title = db.Column(db.String(80), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    thumbnail = db.Column(db.String(20), nullable=False, default="default.jpeg")
    videos = db.relationship("Video", secondary=vid_pl_association, backref="playlists", lazy=True)

    def __repr__(self):
        return f"Playlist('{self.id}', '{self.title}')"
