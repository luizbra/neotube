from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField, SubmitField
from wtforms.validators import DataRequired, EqualTo, Length, ValidationError

from neotube.models import User


class RegisterForm(FlaskForm):
    username = StringField(
        "Username:", validators=[DataRequired(), Length(min=5, max=15)]
    )
    password = PasswordField("Password:", validators=[DataRequired(), Length(min=5)])
    confirm_password = PasswordField(
        "Confirm Password:", validators=[DataRequired(), EqualTo("password")]
    )
    submit = SubmitField("Sign Up")

    def validate_username(self, username):
        if User.query.filter_by(username=username.data).first():
            raise ValidationError("An user with chosen username already exits!")


class LoginForm(FlaskForm):
    username = StringField("Username:", validators=[DataRequired()])
    password = PasswordField("Password:", validators=[DataRequired()])
    remember = BooleanField("Remember Me")
    submit = SubmitField("Sign In")
