from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user

from neotube import bcrypt, db
from neotube.models import User, Video, Playlist
from neotube.users.forms import LoginForm, RegisterForm

users = Blueprint("users", __name__)


@users.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("videos.index"))
    form = RegisterForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )
        db.session.add(User(username=form.username.data, password=hashed_password))
        db.session.commit()
        flash(
            f"Account created for '{form.username.data}'! You may now login.", "success"
        )
        return redirect(url_for("users.login"))
    return render_template("users/register.html", title="Register", form=form)


@users.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("videos.index"))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get("next")
            if next_page:
                return redirect(next_page)
            return redirect(url_for("videos.index"))
        else:
            flash(f"Bad credentials!", "danger")
    return render_template("users/login.html", title="Login", form=form)


@users.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("videos.index"))


@users.route("/user/<string:username>")
def user_videos(username):
    user = User.query.filter_by(username=username).first_or_404()
    videos = Video.query.filter_by(user=user).order_by(Video.date.desc())
    if current_user == user:
        videos = videos.paginate(page=request.args.get("page", 1, type=int), per_page=11)
    else:
        videos = videos.paginate(page=request.args.get("page", 1, type=int), per_page=12)

    return render_template("users/user_videos.html", videos=videos, user=user, playlists=len(user.playlists))

@users.route("/user/<string:username>/playlists")
def user_playlists(username):
    user = User.query.filter_by(username=username).first_or_404()
    playlists = Playlist.query.filter_by(user=user).order_by(Playlist.date.desc())
    if current_user == user:
        playlists = playlists.paginate(page=request.args.get("page", 1, type=int), per_page=11)
    else:
        playlists = playlists.paginate(page=request.args.get("page", 1, type=int), per_page=12)
    return render_template("users/user_playlists.html", videos=len(user.videos), user=user, playlists=playlists)
