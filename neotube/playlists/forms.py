from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from wtforms import StringField, SubmitField, SelectMultipleField
from wtforms.validators import DataRequired


class NewForm(FlaskForm):
    title = StringField("Title:", validators=[DataRequired()])
    thumbnail = FileField(
        "Thumbnail: (Optional)",
        validators=[FileAllowed(["jpg", "jpeg", "png", "webp"])],
    )
    ids = SelectMultipleField("Select the videos:", coerce=int, validators=[DataRequired()])
    submit = SubmitField("Create")


class EditForm(FlaskForm):
    title = StringField("Title:", validators=[DataRequired()])
    thumbnail = FileField(
        "Thumbnail:",
        validators=[FileAllowed(["jpg", "jpeg", "png", "webp"])],
    )
    ids = SelectMultipleField("Select the videos:", coerce=int, validators=[DataRequired()])
    submit = SubmitField("Edit")
