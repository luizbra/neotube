import os

from flask import current_app, Blueprint, flash, redirect, render_template, url_for, request
from flask_login import current_user, login_required

from neotube import db
from neotube.models import Playlist, Video
from neotube.playlists.forms import EditForm, NewForm
from neotube.playlists.utils import save_thumb

playlists = Blueprint("playlists", __name__)

@playlists.route("/playlist/<int:playlist_id>")
def playlist(playlist_id):
    playlist = Playlist.query.get_or_404(playlist_id)
    videos = Video.query.filter(Video.playlists.contains(playlist)).order_by(Video.date.desc()).paginate(page=request.args.get("page", 1, type=int), per_page=12)
    return render_template("playlists/playlist.html", title=playlist.title, videos=videos, playlist=playlist)

@playlists.route("/playlist/new", methods=["GET", "POST"])
@login_required
def new_playlist():
    if not current_user.videos:
        flash(f"You need to upload a video first.", "danger")
        return redirect(url_for("videos.upload"))
    form = NewForm()
    form.ids.choices = [(vid.id, vid.title) for vid in current_user.videos]
    if form.validate_on_submit():
        playlist = Playlist(title=form.title.data, user=current_user)
        db.session.add(playlist)
        db.session.commit()
        if form.thumbnail.data:
            playlist.thumbnail = save_thumb(f"{playlist.id}.jpeg", form.thumbnail.data)
        for video_id in form.ids.data:
            video = Video.query.get(video_id)
            video.playlists.append(playlist)
        db.session.commit()
        flash(f"'{playlist.title}' has been successfully created!", "success")
        return redirect(url_for("users.user_playlists", username=current_user.username))
    return render_template("playlists/new_playlist.html", form=form, title="New Playlist")

@playlists.route("/playlist/<int:playlist_id>/edit", methods=["GET", "POST"])
@login_required
def edit_playlist(playlist_id):
    playlist = Playlist.query.get_or_404(playlist_id)
    if playlist.user != current_user:
        abort(403)
    form = EditForm()
    form.ids.choices = [(vid.id, vid.title) for vid in playlist.user.videos]
    if form.validate_on_submit():
        playlist.title = form.title.data
        if form.thumbnail.data:
            playlist.thumbnail = save_thumb(f"{video.id}.jpeg", form.thumbnail.data)
        playlist.videos = []
        for video_id in form.ids.data:
            video = Video.query.get(video_id)
            playlist.videos.append(video)
        db.session.commit()
        flash(f"'{playlist.title}' has been successfully updated!", "success")
        return redirect(url_for("playlists.playlist", playlist_id=playlist.id))
    elif request.method == "GET":
        form.title.data = playlist.title
    return render_template("playlists/edit_playlist.html", title="Edit", form=form)


@playlists.route("/playlist/<int:playlist_id>/delete")
@login_required
def delete_playlist(playlist_id):
    playlist = Playlist.query.get_or_404(playlist_id)
    if playlist.user != current_user:
        abort(403)
    db.session.delete(playlist)
    db.session.commit()
    if playlist.thumbnail != "default.jpeg":
        path = os.path.join(current_app.root_path, "static/playlists", playlist.thumbnail)
        if os.path.isfile(path):
            os.remove(path)
    flash(f"'{playlist.title}' has been successfully deleted!", "success")
    return redirect(url_for("users.user_playlists", username=current_user.username))
