import secrets

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_minify import minify
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
bcrypt = Bcrypt()
login_manager = LoginManager()
login_manager.login_view = "users.login"
login_manager.login_message_category = "info"


def create_app():
    app = Flask(__name__)
    app.config["SECRET_KEY"] = secrets.token_hex(18)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite3"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    from neotube.errors.handlers import errors
    from neotube.users.routes import users
    from neotube.videos.routes import videos
    from neotube.playlists.routes import playlists

    app.register_blueprint(users)
    app.register_blueprint(videos)
    app.register_blueprint(errors)
    app.register_blueprint(playlists)

    # minify(app=app, html=True, js=True, cssless=True)

    return app
