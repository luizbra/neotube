import os

import markdown
from flask import (
    Blueprint,
    abort,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user, login_required

from neotube import db
from neotube.models import Video
from neotube.videos.forms import EditForm, UploadForm
from neotube.videos.utils import save_thumb

videos = Blueprint("videos", __name__)


@videos.route("/")
def index():
    query = request.args.get("q")
    if query:
        return render_template(
            "videos/index.html",
            videos=Video.query.filter(Video.title.contains(query)).paginate(
                page=request.args.get("page", 1, type=int), per_page=12
            ),
        )
    return render_template(
        "videos/index.html",
        videos=Video.query.order_by(Video.date.desc()).paginate(
            page=request.args.get("page", 1, type=int), per_page=12
        ),
    )


@videos.route("/video/<int:video_id>")
def video(video_id):
    video = Video.query.get_or_404(video_id)
    if video.description:
        video.description = markdown.markdown(video.description)
    return render_template("videos/video.html", title=video.title, video=video)


@videos.route("/video/upload", methods=["GET", "POST"])
@login_required
def upload():
    form = UploadForm()
    if form.validate_on_submit():
        video = Video(
            title=form.title.data,
            description=form.description.data,
            video="_",
            user=current_user,
        )
        db.session.add(video)
        db.session.commit()
        filename = f"{video.id}.mp4"
        form.video.data.save(
            os.path.join(current_app.root_path, "static/videos", filename)
        )
        video.video = filename
        if form.thumbnail.data:
            video.thumbnail = save_thumb(f"{video.id}.jpeg", form.thumbnail.data)
        db.session.commit()
        flash(f"'{form.title.data}' has been successfully uploaded!", "success")
        return redirect(url_for("users.user_videos", username=current_user.username))
    return render_template("videos/upload.html", form=form, title="Upload")


@videos.route("/video/<int:video_id>/edit", methods=["GET", "POST"])
@login_required
def edit_video(video_id):
    video = Video.query.get_or_404(video_id)
    if video.user != current_user:
        abort(403)
    form = EditForm()
    if form.validate_on_submit():
        video.title = form.title.data
        video.description = form.description.data
        if form.thumbnail.data:
            video.thumbnail = save_thumb(f"{video.id}.jpeg", form.thumbnail.data)
        db.session.commit()
        flash(f"'{video.title}' has been successfully updated!", "success")
        return redirect(url_for("videos.video", video_id=video.id))
    elif request.method == "GET":
        form.title.data = video.title
        form.description.data = video.description
    return render_template("videos/edit.html", title="Edit", form=form)


@videos.route("/video/<int:video_id>/delete")
@login_required
def delete_video(video_id):
    video = Video.query.get_or_404(video_id)
    if video.user != current_user:
        abort(403)
    db.session.delete(video)
    db.session.commit()
    path = os.path.join(current_app.root_path, "static/videos", video.video)
    if os.path.isfile(path):
        os.remove(path)
    if video.thumbnail != "default.jpeg":
        path = os.path.join(current_app.root_path, "static/videos", video.thumbnail)
        if os.path.isfile(path):
            os.remove(path)
    flash(f"'{video.title}' has been successfully deleted!", "success")
    return redirect(url_for("users.user_videos", username=current_user.username))
