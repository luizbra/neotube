from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField, FileRequired
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired


class UploadForm(FlaskForm):
    title = StringField("Title:", validators=[DataRequired()])
    description = TextAreaField("Description: (Optional)")
    thumbnail = FileField(
        "Thumbnail: (Optional)",
        validators=[FileAllowed(["jpg", "jpeg", "png", "webp"])],
    )
    video = FileField("Video:", validators=[FileAllowed(["mp4"]), FileRequired()])
    submit = SubmitField("Upload")


class EditForm(FlaskForm):
    title = StringField("Title:", validators=[DataRequired()])
    description = TextAreaField("Description:")
    thumbnail = FileField(
        "Thumbnail:",
        validators=[FileAllowed(["jpg", "jpeg", "png", "webp"])],
    )
    submit = SubmitField("Edit")
