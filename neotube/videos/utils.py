import os

from flask import current_app
from PIL import Image


def save_thumb(filename, image):
    thumbnail = Image.open(image)
    thumbnail = thumbnail.resize((640, 360))
    thumbnail.save(os.path.join(current_app.root_path, "static/videos", filename))

    return filename
