import os
from datetime import datetime

import pytest

from neotube import create_app, db


@pytest.fixture
def client():
    app = create_app()
    app.config["TESTING"] = True
    app.config["WTF_CSRF_ENABLED"] = False

    db.drop_all(app=app)
    db.create_all(app=app)

    with app.test_client() as client:
        yield client


def register(client, username, password):
    return client.post(
        "/register",
        data=dict(
            username=username,
            password=password,
            confirm_password=password,
        ),
        follow_redirects=True,
    )


def login(client, username, password):
    return client.post(
        "/login", data=dict(username=username, password=password), follow_redirects=True
    )


def get_user(client, username):
    return client.get(f"/user/{username}", follow_redirects=True)


def logout(client):
    return client.get("/logout", follow_redirects=True)


def upload(client, title, description=None):
    return client.post(
        "/video/upload",
        data=dict(
            title=title,
            description=description,
            thumbnail=open(
                os.path.join(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)), "video.webp"
                    )
                ),
                "rb",
            ),
            video=open(
                os.path.join(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)), "video.mp4"
                    )
                ),
                "rb",
            ),
        ),
        follow_redirects=True,
    )

def edit(client, video_id, title, description=None):
    return client.post(
        f"/video/{video_id}/edit",
        data=dict(title=title, description=description),
        follow_redirects=True,
    )


def delete(client, video_id):
    return client.get(f"/video/{video_id}/delete", follow_redirects=True)


def get_404(client):
    return client.get("/BAZINGA!", follow_redirects=True)

def get_page(client, page):
    return client.get(page, follow_redirects=True)

def new_playlist(client, title, max_id):
    return client.post(
        "/playlist/new",
        data=dict(
            title=title,
            thumbnail=open(
                os.path.join(
                    os.path.join(
                        os.path.dirname(os.path.realpath(__file__)), "video.webp"
                    )
                ),
                "rb",
            ),
            ids=list(range(1, max_id)),
        ),
        follow_redirects=True,
    )

def edit_playlist(client, playlist_id, title, max_id):
    return client.post(
        f"/playlist/{playlist_id}/edit",
        data=dict(title=title, ids=list(range(1, max_id))),
        follow_redirects=True,
    )

def del_playlist(client, playlist_id):
    return client.get(f"/playlist/{playlist_id}/delete", follow_redirects=True)

username = "admin"
password = "admin"
title = "Hello, world!"
description = "Foo bar!"


def test_users(client):
    rv = login(client, "bad_credentials", password)
    assert b"Bad credentials!" in rv.data

    rv = register(client, username, password)
    assert b"You may now login." in rv.data
    assert (
        f"Account created for &#39;{username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, username, password)
    assert b"Sign Out" in rv.data

    rv = logout(client)
    assert b"Sign Up" in rv.data

    rv = get_user(client, username)
    assert rv.status_code == 200


def test_videos(client):
    new_title = "Hello, John!"

    rv = register(client, username, password)
    assert (
        f"Account created for &#39;{username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, username, password)
    assert b"Sign Out" in rv.data

    rv = upload(client, title=title, description=description)
    assert f"&#39;{title}&#39; has been successfully uploaded!" in rv.data.decode(
        "utf-8"
    )

    rv = edit(client, video_id=1, title=new_title, description=description)
    assert f"&#39;{new_title}&#39; has been successfully updated!" in rv.data.decode(
        "utf-8"
    )

    rv = delete(client, video_id=1)
    assert f"&#39;{new_title}&#39; has been successfully deleted!" in rv.data.decode(
        "utf-8"
    )


def test_errors(client):
    new_username = "jerry"
    new_password = "jerry"

    rv = get_404(client)
    assert rv.status_code == 404
    assert b"404 Not Found" in rv.data

    rv = register(client, username, password)
    assert (
        f"Account created for &#39;{username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, username, password)
    assert b"Sign Out" in rv.data

    rv = upload(client, title=title, description=description)
    assert f"&#39;{title}&#39; has been successfully uploaded!" in rv.data.decode(
        "utf-8"
    )

    rv = logout(client)
    assert b"Sign Up" in rv.data

    rv = register(client, new_username, new_password)
    assert (
        f"Account created for &#39;{new_username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, new_username, new_password)
    assert b"Sign Out" in rv.data

    rv = delete(client, video_id=1)
    assert rv.status_code == 403
    assert b"403 Forbidden" in rv.data


def test_playlists(client):
    title = "Salty Playlist"
    new_title = "Saltless Playlist"

    rv = register(client, username, password)
    assert (
        f"Account created for &#39;{username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, username, password)
    assert b"Sign Out" in rv.data

    rv = upload(client, title="Hello, there!", description=description)
    assert f"&#39;Hello, there!&#39; has been successfully uploaded!" in rv.data.decode(
        "utf-8"
    )

    rv = new_playlist(client, title=title, max_id=2)
    assert f"&#39;{title}&#39; has been successfully created!" in rv.data.decode(
        "utf-8"
    )

    rv = edit_playlist(client, playlist_id=1, title=new_title, max_id=2)
    assert f"&#39;{new_title}&#39; has been successfully updated!" in rv.data.decode(
        "utf-8"
    )

    rv = del_playlist(client, playlist_id=1)
    assert f"&#39;{new_title}&#39; has been successfully deleted!" in rv.data.decode(
        "utf-8"
    )

def test_pagination(client):
    pl_title="Nice Playlist"

    rv = register(client, username, password)
    assert (
        f"Account created for &#39;{username}&#39;! You may now login."
        in rv.data.decode("utf-8")
    )

    rv = login(client, username, password)
    assert b"Sign Out" in rv.data

    for i in range(13):
        rv = upload(client, title=title, description=description)
        assert f"&#39;{title}&#39; has been successfully uploaded!" in rv.data.decode(
            "utf-8"
        )

    rv = get_page(client, f"/user/{username}?page=2")
    assert rv.status_code == 200

    rv = get_page(client, f"/user/{username}?page=3")
    assert rv.status_code == 404


    for i in range(13):
        rv = new_playlist(client, title=pl_title, max_id=14)
        assert f"&#39;{pl_title}&#39; has been successfully created!" in rv.data.decode(
            "utf-8"
        )

    rv = get_page(client, f"/user/{username}/playlists?page=2")
    assert rv.status_code == 200

    rv = get_page(client, f"/user/{username}/playlists?page=3")
    assert rv.status_code == 404

    rv = get_page(client, f"/playlist/1?page=2")
    assert rv.status_code == 200

    rv = get_page(client, f"/playlist/1?page=3")
    assert rv.status_code == 404
