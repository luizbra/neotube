from flask import Blueprint, render_template

errors = Blueprint("errors", __name__)


@errors.app_errorhandler(404)
def not_found(error):
    return render_template("errors/error.html", title="404 Not Found"), 404


@errors.app_errorhandler(403)
def forbidden(error):
    return render_template("errors/error.html", title="403 Forbidden"), 403


@errors.app_errorhandler(500)
def server_error(error):
    return render_template("errors/error.html", title="500 Server Error"), 500
